import socket
import random
import numpy
import time
from bitstring import BitArray
import struct
#import Adafruit_BBIO.GPIO as GPIO
#import Adafruit_BBIO.PWM as PWM
import warnings
warnings.filterwarnings("ignore")

def write_Coil(MB_data1,Coil0,Coil1,outPin0,outPin1):
	if (len(MB_data1)<100):
		print ("No challenge received")
		noChallenge=1
	else:
		noChallenge=0
		CoilID=MB_data1[64:80]
		CoilValue=MB_data1[80:88]
		if (CoilID=='0x0000')&(CoilValue=='0x00'):
			Coil0=False
			#GPIO.output(outPin0,GPIO.LOW)
		if (CoilID=='0x0000')&(CoilValue=='0xff'):
			Coil0=True
			#GPIO.output(outPin0,GPIO.HIGH)
		if (CoilID=='0x0001')&(CoilValue=='0x00'):
			Coil1=False
			#GPIO.output(outPin1,GPIO.LOW)
		if (CoilID=='0x0001')&(CoilValue=='0xff'):
			Coil1=True
			#GPIO.output(outPin1,GPIO.HIGH)
		print('Coil 0 : ',Coil0)
		print('Coil 1 : ',Coil1)
	answer1=str(MB_data1[0:96])
	return Coil0,Coil1,answer1,noChallenge;

def write_Register(MB_data1,outPin2):
	if (len(MB_data1)<100):
		print ("No challenge received")
		noChallenge=1
		answer1=str(MB_data1[0:96])
	else:
		#PWM.start(outPin2,0,1000)
		#PWM.start(outPin1,0,1000)
		noChallenge=0
		RegisterOffset=MB_data1[64:80]
		print("Register offset: ",RegisterOffset)
		RegisterValue=MB_data1[80:96]
		print("Register Value: ",RegisterValue)
		RegisterValue1=str(RegisterValue)
		RegisterValue2=RegisterValue1.replace("0x","")
		RegisterValueINT=int(RegisterValue2,16)
		#print("Register Value(int): ",RegisterValueINT)
		#RegisterValueFLOAT=struct.unpack('f',RegisterValue)
		RegisterValueFLOAT=(float(RegisterValueINT))/655
		#print("Register Value (float): ",RegisterValueFLOAT)
		if (RegisterOffset=='0x0002'):
			#PWM.set_duty_cycle(outPin2,RegisterValueFLOAT)
			answer1=str(MB_data1[0:96])
		elif (RegisterOffset=='0x0001'):
			#PWM.set_duty_cycle(outPin1,RegisterValueFLOAT)
			answer1=str(MB_data1[0:96])
		else:
			print ("Invalid Register")
			MB_data2=MB_data1
			MB_data2[56:64]='0x81'
			MB_data2[64:72]='0x02'
			answer1=str(MB_data2[0:72])

	return answer1,noChallenge;

def extract_ans(MB_RX,key_x_bin,bits_challenge):
	ans_data=MB_RX[96:128] #ans_data=MB_RX[96:120]
	ans_data2=str(ans_data)
	ans_data3=ans_data2.replace("0x","")
	ans_data5=bin(int(ans_data3,16))[2:].zfill(bits_challenge)
	b__bin = numpy.fromstring(str(ans_data5),'u1') - ord('0')
	b_bin=b__bin^key_x_bin
	return b_bin;

def answer_gen (key_x_bin,challengeDEC,bits_challenge,challenge,key_x):
	mask=0b1111000000000000000000000000000 #mask=0b11110000000000000000000 #necessary to obtain the 4 most significant bits of a
	rotation_a = (mask & challengeDEC) >> (bits_challenge-4)
	rol = lambda val, r_bits, max_bits: \
		  (val << r_bits%max_bits) & (2**max_bits-1) | \
		  ((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))

	a_rot = rol(challengeDEC, rotation_a, bits_challenge)
	a_rot_bin=[int(a_rot_bin) for a_rot_bin in bin(a_rot)[2:]]
	pad=bits_challenge-len(a_rot_bin)
	a_rot_bin=numpy.pad(a_rot_bin,(pad,0),'constant',constant_values=(0, 0))
	rotation_s_aux = a_rot ^ key_x
	rotation_s = (mask & rotation_s_aux) >> (bits_challenge-4)
	s = rol(key_x, rotation_s, bits_challenge)
	s_bin=[int(s_bin) for s_bin in bin(s)[2:]]
	pad=bits_challenge-len(s_bin)
	s_bin=numpy.pad(s_bin,(pad,0),'constant',constant_values=(0, 0))
	z1=(challenge|a_rot_bin)
	#####z1=numpy.dot(challenge,a_rot_bin)
	z2=(key_x_bin|s_bin)
	#####z2=numpy.dot(key_x_bin,s_bin)
	z_bin=z1^z2
	ind=0
	z=0
	for ind in range(0,bits_challenge):
		z=z_bin[bits_challenge-1-ind]*(2**ind)+z

	return s_bin,z,z_bin; #####return s_bin,z;

def answer_noise(z,bits_challenge,s_bin,key_x_bin,z_bin):
	#vetor1=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
	count=0
	b_check=0
	while (b_check == 0 and count < 10):
		count=count+1
		ansB=0 #answer to be sent, to be verified with z (is keyX DOT S equals b DOT S?)
		ansBbin=[int(ansBbin) for ansBbin in bin(ansB)[2:]]
		#print(ansBbin)
		ansBbin=numpy.pad(ansBbin,(bits_challenge-1,0),'constant',constant_values=(0, 0))
		#print(ansBbin)
		for i in range(0,bits_challenge-1):
			ansBbin[i+1]=int(round(random.random()+0.3,0))
			#print(z)
			#		print(ansBbin)
			#		print(s_bin)
			verify = numpy.dot(ansBbin,s_bin)
			verify2=sum(z_bin)
			if (verify==verify2):
				b_check=1
				break

	#for i in range(0,bits_challenge-1):
	#	verify=numpy.dot(ansBbin,s_bin)
	#	if (verify!=z):
	#		ansBbin[i]=s_bin[i]
	#print(z)
	#print(ansBbin)
	#print(s_bin)
	bXOR=ansBbin^key_x_bin
	ind=0
	bDEC=0
	for ind in range(0, bits_challenge): #conversao em inteiro do vetor (que nao está no formato binário, mas representa um binário)
		bDEC=bXOR[bits_challenge-1-ind]*(2**ind)+bDEC

	bXORhex=hex(bDEC)
	bXORhex1=bXORhex.replace('0x','').zfill(8) #fromhex nao funciona se nao tiver quantidade par de dígitos
	bBYTES=bytes.fromhex(bXORhex1) #de acordo com https://stackoverflow.com/questions/8904092/how-can-i-send-anything-other-than-strings-through-python-sock-send
	return bBYTES;

def challenge_gen(key_x_bin, bits_challenge, key_x,count,MasterBbytes): #Create the challenge HB-MP*
	challengeDEC=0
	if (count==0):
		challenge=[int(challenge) for challenge in bin(challengeDEC)[2:]]
		challenge=numpy.pad(challenge,(bits_challenge-1,0),'constant',
						constant_values=(0, 0))
		for i in range(0,bits_challenge-1):
			challenge[i]=int(round(random.random(),0)) #generates a random vector of 0's and 1's for the original challenge

	else:
		challenge_data=BitArray(MasterBbytes)
		challenge_data2=str(challenge_data)
		challenge_data3=challenge_data2.replace("0x","")
		challenge_data5=bin(int(challenge_data3,16))[2:].zfill(bits_challenge)
		challenge = numpy.fromstring(str(challenge_data5),'u1') - ord('0')
		challenge=key_x_bin ^ challenge

	#print('Binary challenge: ',challenge)
	for bit in challenge:
		challengeDEC = (challengeDEC << 1) | bit #converts the random challenge in decimal

	challengeXOR=key_x_bin ^ challenge #this is the challenge' (challenge_)
	ind=0
	challengeXORdec=0
	for ind in range(0, bits_challenge): #conversao em inteiro do vetor (que nao está no formato binário, mas representa um binário)
		challengeXORdec=challengeXOR[bits_challenge-1-ind]*(2**ind)+challengeXORdec

	challengeXORhex=hex(challengeXORdec)
	challengeXORhex1=challengeXORhex.replace('0x','').zfill(8) #fromhex nao funciona se nao tiver quantidade par de dígitos
	challengeBYTES=bytes.fromhex(challengeXORhex1) #de acordo com https://stackoverflow.com/questions/8904092/how-can-i-send-anything-other-than-strings-
	return challenge,challengeDEC,challengeXOR,challengeXORdec,challengeBYTES;

outPin0="P9_12"
outPin1="P9_15"
outPin2="P8_13"
#Button1="P9_25"
#GPIO.setup(outPin0,GPIO.OUT)
#GPIO.setup(outPin1,GPIO.OUT)
#GPIO.setup(Button0,GPIO.IN)
#GPIO.setup(Button1,GPIO.IN)
key_x_bin=[1,1,0, 1,0,1,0, 1,1,1,0, 0,0,0,1, 0,1,1,1, 1,0,0,1, 1,1,0,1, 0,1,1,1]#key_x_bin=[1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1,0,1,0,1,0,1]
bits_challenge=31 #bits_challenge=23
key_x=1793161687 #key_x = 5969621
SlaveID='0x01'
Coil0=False
Coil1=False
server=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#slaveIP=int(input('Connection (1 for USB, 2 for Ethernet): '))
#if (slaveIP==2):
#	ip='192.168.100.115' #ethernet connection
#if (slaveIP==1):
#	ip='192.168.7.2' #usb connection
#if (slaveIP==3):
#    ip='172.30.32.1' #docker connection
#if (slaveIP==4):
#    ip='192.168.100.101' #docker connection
#if (slaveIP==5):
#    ip='0.0.0.0' #docker connection
ip='0.0.0.0'
port=502
address=(ip,port)
print(address,"    address")
#server.close()
server.bind(address)
server.listen(5)
print ("Started listening ", ip, ": ",port)
count=0
incorrect=0

while True:
	try:
		#server=socket.socket(socket.AF_INET,socket.SOCK_STREAM)		#address=(ip,port)		#server.bind(address)		#server.listen(1)		#print ("Started listening ", ip, ": ",port)
		client,addr=server.accept()
		#print ("Got a connection from ", addr[0])
		try:
			while True:
				MB_data=client.recv(1024)
				#start_conn=time.clock() #connection started
				print (MB_data, " Data received\n")
				break
		finally:
			None

		MB_data1=BitArray(MB_data)
		ByteCounter=MB_data1[40:48]
		UnitID=MB_data1[48:56]
		if (UnitID==SlaveID):
			FunctionCode=MB_data1[56:64]
			if (FunctionCode=='0x05'):
				Coil0,Coil1,answer1,noChallenge=write_Coil(MB_data1,Coil0,Coil1,outPin0,outPin1)

			if (FunctionCode=='0x06'):
                                answer1,noChallenge=write_Register(MB_data1,outPin2)

		if (noChallenge==0):
			#time_MB=time.clock() #Modbus request interpretation ends and challenge HB interpretation starts	#challenge_data=MB_data1[96:120]			#challenge_data2=str(challenge_data)			#challenge_data3=challenge_data2.replace("0x","")			#challenge_data5=bin(int(challenge_data3,16))[2:].zfill(17)			#challenge_ = numpy.fromstring(str(challenge_data5),'u1') - ord('0')			#print('Challenge received: ', challenge_)			#challenge_bin=challenge_^key_x_bin
			challenge_bin=extract_ans(MB_data1,key_x_bin,bits_challenge) #MB_data1 already in bit array)
			#Now to verify the authenticity of the master
			if (count>0):
				challenge,challengeDEC,challengeXOR,challengeXORdec,challengeBYTES=challenge_gen(key_x_bin,bits_challenge, key_x,count,data)
				s_bin,z,z_bin=answer_gen (key_x_bin,challengeDEC,bits_challenge,challenge,key_x)
				check=numpy.dot(challenge_bin,s_bin)
				#print(check, " - Resposta recebida multiplicada pelo S\n", sum(z_bin), " - Soma interna do vetor z")
				if (check == sum(z_bin)):
					incorrect=incorrect
					print("\nAuthentic node. ", incorrect, " incorrect answer(s) in ", count, " challenge(s)\n")
				else:
					incorrect=incorrect+1
					print("\nNot authentic node. ", incorrect, "incorrect answer(s) in ", count, "challenge(s)\n")
			challenge_d=0
			for bit in challenge_bin:
				challenge_d = (challenge_d << 1) | bit
				#mask=0b11110000000000000 #necessary to obtain the 4 most significant bits of a			rotation_a = (mask & challenge_d) >> 13			rol = lambda val, r_bits, max_bits: \				(val << r_bits%max_bits) & (2**max_bits-1) | \				((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))			a_rot = rol(challenge_d, rotation_a, bits_challenge)			a_rot_bin=[int(a_rot_bin) for a_rot_bin in bin(a_rot)[2:]]			if len(a_rot_bin)<bits_challenge:				pad=bits_challenge-len(a_rot_bin)				a_rot_bin=numpy.pad(a_rot_bin,(pad,0),'constant',constant_values=(0, 0))			rotation_s_aux = a_rot ^ key_x			rotation_s = (mask & rotation_s_aux) >> 13			s = rol(key_x, rotation_s, bits_challenge)			s_bin=[int(s_bin) for s_bin in bin(s)[2:]]			if len(s_bin)<bits_challenge:				pad=bits_challenge-len(s_bin)				s_bin=numpy.pad(s_bin,(pad,0),'constant',constant_values=(0, 0))			z1=numpy.dot(challenge_bin,a_rot_bin)			z2=numpy.dot(key_x_bin,s_bin)			z=z1^z2			#noise=int(round(random.random()*0.6,0))			noise=0			z=z^noise
			s_bin,z,z_bin=answer_gen(key_x_bin,challenge_d,bits_challenge,challenge_bin,key_x)
			data=answer_noise(z,bits_challenge,s_bin,key_x_bin,z_bin)
			#count=0			b_check=0			while (b_check == 0 and count < 100):				count=count+1				b=0				b_bin=[int(b_bin) for b_bin in bin(b)[2:]]				b_bin=numpy.pad(b_bin,(bits_challenge-1,0),'constant',constant_values=(0, 0))				for i in range(0,bits_challenge-1):					b_bin[i]=int(round(random.random()+0.3,0))					verify = numpy.dot(b_bin,s_bin)					if (verify==z):						b_check=1						break
			#answer_b=b_bin^key_x_bin			ind=0			answer_int=0			for ind in range(0, 17): #conversao em inteiro do vetor (que nao es no formato binrio, mas representa um binrio)				answer_int=answer_b[16-ind]*(2**ind)+answer_int			answer_hex=hex(answer_int)			answer_hex1=answer_hex.replace('0x','').zfill(6) #fromhex nao funciona se nao tiver quantidade par de dgitos			data=bytes.fromhex(answer_hex1) #de acordo com https://stackoverflow.com/questions/8904092/how-can-i-send-anything-other-than-strings-through-python-sock-send
			#print('Answer to challenge: ', data)
			#answer1=str(MB_data1[0:96])
			answer2=answer1.replace('0x','')
			answer3=bytes.fromhex(answer2)
			print (answer3+data, ' - Data sent\n')
			try:
				while True:
					client.send(answer3 + data)
					break
			finally:
				None

			count=count+1
			#data_rcv=client.recv(1024)

	finally:
		None
		#print('\nCLOSING SOCKET')		#server.close()

	#GPIO.cleanup()
	#PWM.stop(outPin2)
	#PWM.cleanup()


