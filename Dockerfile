FROM python:3

EXPOSE 502

ADD my_script.py /

RUN pip install numpy

RUN pip install bitstring

CMD [ "python", "./my_script.py" ]