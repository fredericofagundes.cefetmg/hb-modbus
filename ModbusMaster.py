#updates:
#multiple slaves communication
#challenge and answers are stored or not (configurable). communication with legacy slaves does not occupy memory for HB-MP*
#separate vectors for each slave (matrix did not work)
#31 bits for HB-MP*

import socket
import struct
import time
import random
import numpy
from bitstring import BitArray
import warnings
warnings.filterwarnings("ignore")

bits_challenge=31
key_x_bin=[1,1,0, 1,0,1,0, 1,1,1,0, 0,0,0,1, 0,1,1,1, 1,0,0,1, 1,1,0,1, 0,1,1,1]
key_x = 1793161687

def challenge_gen(key_x_bin, bits_challenge, key_x,count,MasterBbytes,noChallenge): #Create the challenge HB-MP*
    challengeDEC=0
    if (count==0 or noChallenge==1):
        challenge=[int(challenge) for challenge in bin(challengeDEC)[2:]]
        challenge=numpy.pad(challenge,(bits_challenge-1,0),'constant',
                        constant_values=(0, 0))
        for i in range(0,bits_challenge-1):
            challenge[i]=int(round(random.random(),0)) #generates a random vector of 0's and 1's for the challenge
            
    else:
        challenge_data=BitArray(MasterBbytes)
        challenge_data2=str(challenge_data)
        challenge_data3=challenge_data2.replace("0x","")
        challenge_data5=bin(int(challenge_data3,16))[2:].zfill(bits_challenge)
        challenge = numpy.fromstring(str(challenge_data5),'u1') - ord('0')
        challenge=key_x_bin ^ challenge

    for bit in challenge:
        challengeDEC = (challengeDEC << 1) | bit #converts the random challenge in decimal

    challengeXOR=key_x_bin ^ challenge #this is the challenge A'
    ind=0
    challengeXORdec=0
    for ind in range(0, bits_challenge): #vector conversion to integer (it is not binary, but represents a binary)
        challengeXORdec=challengeXOR[bits_challenge-1-ind]*(2**ind)+challengeXORdec

    challengeXORhex=hex(challengeXORdec)
    challengeXORhex1=challengeXORhex.replace('0x','').zfill(8) #fromhex did not work with odd number of digits
    challengeBYTES=bytes.fromhex(challengeXORhex1) #according to https://stackoverflow.com/questions/8904092/how-can-i-send-anything-other-than-strings-
    return challenge,challengeDEC,challengeXOR,challengeXORdec,challengeBYTES;
    
def answer_gen (key_x_bin,challengeDEC,bits_challenge,challenge,key_x):
    mask=0b1111000000000000000000000000000 #necessary to obtain the 4 most significant bits of a
    rotation_a = (mask & challengeDEC) >> (bits_challenge-4)
    rol = lambda val, r_bits, max_bits: \
          (val << r_bits%max_bits) & (2**max_bits-1) | \
          ((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))

    a_rot = rol(challengeDEC, rotation_a, bits_challenge)
    a_rot_bin=[int(a_rot_bin) for a_rot_bin in bin(a_rot)[2:]]
    pad=bits_challenge-len(a_rot_bin)
    a_rot_bin=numpy.pad(a_rot_bin,(pad,0),'constant',constant_values=(0, 0))
    rotation_s_aux = a_rot ^ key_x
    rotation_s = (mask & rotation_s_aux) >> (bits_challenge-4)
    s = rol(key_x, rotation_s, bits_challenge)
    s_bin=[int(s_bin) for s_bin in bin(s)[2:]]
    pad=bits_challenge-len(s_bin)
    s_bin=numpy.pad(s_bin,(pad,0),'constant',constant_values=(0, 0))
    z1=(challenge|a_rot_bin)
    z2=(key_x_bin|s_bin)
    z_bin=z1^z2 
    ind=0
    z=0
    for ind in range(0, bits_challenge): 
        z=z_bin[bits_challenge-1-ind]*(2**ind)+z
    return s_bin,z,z_bin; 

def answer_noise(z,bits_challenge,s_bin,key_x_bin,z_bin):
    count=0
    b_check=0
    while (b_check == 0 and count < 100):
        count=count+1
        ansB=0 #answer to be sent, to be verified with z (is (keyX DOT S) equal to (b DOT S)?)
        ansBbin=[int(ansBbin) for ansBbin in bin(ansB)[2:]]
        ansBbin=numpy.pad(ansBbin,(bits_challenge-1,0),'constant',constant_values=(0, 0))
        for i in range(0,bits_challenge-1):
            ansBbin[i]=int(round(random.random()+0.3,0))
            verify = numpy.dot(ansBbin,s_bin)
            if (verify==sum(z_bin)): 
                b_check=1
                break

    bXOR=ansBbin^key_x_bin
    ind=0
    bDEC=0
    for ind in range(0, bits_challenge): 
        bDEC=bXOR[bits_challenge-1-ind]*(2**ind)+bDEC

    bXORhex=hex(bDEC)
    bXORhex1=bXORhex.replace('0x','').zfill(8) 
    print ('bXORhex1 = ',bXORhex1)
    bBYTES=bytes.fromhex(bXORhex1) 
    return bBYTES;

def extract_ans(MB_RX,key_x_bin,bits_challenge):
    ans_data=MB_RX[96:128] 
    ans_data2=str(ans_data)
    ans_data3=ans_data2.replace("0x","")
    ans_data5=bin(int(ans_data3,16))[2:].zfill(bits_challenge)
    b__bin = numpy.fromstring(str(ans_data5),'u1') - ord('0')
    b_bin=b__bin^key_x_bin
    print('b_bin ', b_bin)
    return b_bin;

cycles=100
TCP_PORT = 502
BUFFER_SIZE = 39
count1, count2,count3,count4,count5,count6,count7,count8,count9,count10 =0,0,0,0,0,0,0,0,0,0
incorrect1,incorrect2,incorrect3,incorrect4,incorrect5,incorrect6,incorrect7,incorrect8,incorrect9,incorrect10=0,0,0,0,0,0,0,0,0,0
MasterBbytes1,MasterBbytes2,MasterBbytes3,MasterBbytes4,MasterBbytes5,MasterBbytes6,MasterBbytes7,MasterBbytes8,MasterBbytes9,MasterBbytes10=0,0,0,0,0,0,0,0,0,0
noChallenge1,noChallenge2,noChallenge3,noChallenge4,noChallenge5,noChallenge6,noChallenge7,noChallenge8,noChallenge9,noChallenge10=0,0,0,0,0,0,0,0,0,0
automatico=input('\nPerform automatic scan on all slaves? (y/n) ')
tempo_fim_ciclo=[]


for i in range(0,cycles):
    try:
        tempo_inicio_ciclo=time.clock()
        if (automatico=='n'):
            slaveIP=int(input('\nAddress to connect (1 for ModbusPal, 2 BBB1, 3 BBB2, 4 to 8 conteiners): '))
        elif (automatico=='y'):
            slaveIP=(i%8)+1
            
        if (slaveIP==1):
            TCP_IP = '192.168.100.105' #old machine with modbus pal
            registerOffset=int(round(random.random()*5,0))
            TCP_PORT=502
        elif (slaveIP==2):
            TCP_IP = '192.168.100.115' #ethernet connection with BBB1
            registerOffset=2
        elif (slaveIP==3):
            TCP_IP = '192.168.100.116' #ethernet connection with BBB2
            registerOffset=int(round(random.random(),0))
        elif (slaveIP==4):
            TCP_IP = '192.168.100.101' #connection with container 1
            registerOffset=int(round(random.random(),0))+1
            TCP_PORT=8080
        elif (slaveIP==5):
            TCP_IP = '192.168.100.101' #connection with container 2
            registerOffset=int(round(random.random(),0))+1
            TCP_PORT=8082
        elif (slaveIP==6):
            TCP_IP = '192.168.100.101' #connection with container 3
            registerOffset=int(round(random.random(),0))+1
            TCP_PORT=8084
        elif (slaveIP==7):
            TCP_IP = '192.168.100.101' #connection with container 4
            registerOffset=int(round(random.random(),0))+1
            TCP_PORT=8086
        elif (slaveIP==8):
            TCP_IP = '192.168.100.101' #connection with container 5
            registerOffset=int(round(random.random(),0))+1
            TCP_PORT=8088
            
        unitId = 1 
        functionCode = 6 # Write coil
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((TCP_IP, TCP_PORT))
        sock.settimeout(10)
        if (functionCode==5):
            #coilId=int(input('Coil to write (0 or 1): '))
            #coilId=1
            coilId=int(round(random.random(),0)) #random coil
            #value=int(input('Coil value (0 or 1): '))
            #value=1
            value=int(round(random.random(),0)) #random value
            aux_timeAns=time.clock()
            if (value==1):
                req = struct.pack('12B', 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, int(unitId),
                      int(functionCode), 0x00, int(coilId), 0xff, 0x00) # 2B transaction ID (usually 0), 2B protocol identifier (0), 2B lenght (first zero, next is the actual lenght (6 for MB function 5 and 3 for HB),1B for slave or unit ID, 1B for function, next Bytes are data
            else:
                req = struct.pack('12B', 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, int(unitId),
                      int(functionCode), 0x00, int(coilId), 0x00, 0x00)

        if (functionCode==6):  #Write register
            #registerOffset=int(input('Register offset: '))
            registerOffset=2 #fixed register
            #registerValue=int(input('Register value (0 ~ 65535): '))
            registerValue=int(round(random.random()*65535,0)) #random value
            print ("\nRegister Value: ",registerValue)
            high,low=tuple(struct.pack('>H',registerValue))
            req=struct.pack('12B', 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, int(unitId),
                      int(functionCode), 0x00, int(registerOffset), int(high), int(low))
            padding=struct.pack('4B',0x00,0x00,0x00,0x00)#padding=struct.pack('3B',0x00,0x00,0x00)

        #HB_MP=input('Send challenge? (y/n) ') #to configure if HB-MP* data will be sent
        HB_MP='y'
        #SLAVE 1
        if (slaveIP==1):
            if (HB_MP=='y'):
                if (count1==0 or noChallenge1==1):
                    challenge1,challengeDEC1,challengeXOR1,challengeXORdec1,challengeBYTES1 =challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count1,MasterBbytes1,noChallenge1)    
                    sock.send(req+challengeBYTES1) #with challenge
                    print('\nChallenge: ; ',BitArray(challengeBYTES1))
                    s_bin1,z1,z_bin1=answer_gen (key_x_bin,challengeDEC1,bits_challenge,challenge1,key_x)
                if (count1>0 and noChallenge1==0):
                    s_bin1,z1,z_bin1=answer_gen (key_x_bin,bDEC1,bits_challenge,b_bin1,key_x)
                    MasterBbytes1=answer_noise(z1,bits_challenge,s_bin1,key_x_bin,z_bin1)
                    sock.send(req+MasterBbytes1)
                    print('Challenge: ; ', BitArray(MasterBbytes1))
                    challenge1,challengeDEC1,challengeXOR1,challengeXORdec1,challengeBYTES1=challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count1,MasterBbytes1,noChallenge1)
                    s_bin1,z1,z_bin1=answer_gen (key_x_bin,challengeDEC1,bits_challenge,challenge1,key_x)
            else:
                sock.send(req+padding)

            while True:
                rec = sock.recv(BUFFER_SIZE)
                MB_RX1=BitArray(rec)
                break
        
            if (len(MB_RX1)>100):
                if (HB_MP=='y'):
                    noChallenge1=0
                    b_bin1=extract_ans(MB_RX1,key_x_bin,bits_challenge)
                    print('Response to challenge: ; ',MB_RX1[96:128]) 
                    check1=numpy.dot(b_bin1,s_bin1)
                    count1=count1+1
                    if (check1==sum(z_bin1)): 
                        incorrect1=incorrect1
                        print("\nAuthentic node. ", incorrect1, " absent or incorrect answer(s) in ", count1, "challenge(s) (SLAVE 1).\n")
                    else:
                        incorrect1=incorrect1+1
                        print("\nNot authentic node. ", incorrect1, " absent or >incorrect< answer(s) in ", count1, "challenge(s) (SLAVE 1).\n")
                    bDEC1=0
                    for bit in b_bin1:
                        bDEC1 = (bDEC1 << 1) | bit

            else:
                count1=count1+1
                incorrect1=incorrect1+1
                print("\nNot authentic node. ", incorrect1, " >absent< or incorrect answer(s) in ", count1, "challenge(s) (SLAVE 1).\n")
                noChallenge1=1
       #END OF SLAVE 1
            
       #SLAVE 2
        #HB_MP='y'
        if (slaveIP==2):
            if (HB_MP=='y'):
                if (count2==0 or noChallenge2==1):
                    challenge2,challengeDEC2,challengeXOR2,challengeXORdec2,challengeBYTES2 =challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count2,MasterBbytes2,noChallenge2)    
                    sock.send(req+challengeBYTES2) #with challenge
                    print('\nChallenge: ; ',BitArray(challengeBYTES2))
                    s_bin2,z2,z_bin2=answer_gen (key_x_bin,challengeDEC2,bits_challenge,challenge2,key_x)
                if (count2>0 and noChallenge2==0):
                    s_bin2,z2,z_bin2=answer_gen (key_x_bin,bDEC2,bits_challenge,b_bin2,key_x)
                    MasterBbytes2=answer_noise(z2,bits_challenge,s_bin2,key_x_bin,z_bin2)
                    sock.send(req+MasterBbytes2)
                    print('Challenge: ; ', BitArray(MasterBbytes2))
                    challenge2,challengeDEC2,challengeXOR2,challengeXORdec2,challengeBYTES2=challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count2,MasterBbytes2,noChallenge2)
                    s_bin2,z2,z_bin2=answer_gen (key_x_bin,challengeDEC2,bits_challenge,challenge2,key_x)
            else:
                sock.send(req+padding)

            while True:
                rec = sock.recv(BUFFER_SIZE)
                MB_RX2=BitArray(rec)
                break
        
            if (len(MB_RX2)>100):
                if (HB_MP=='y'):
                    noChallenge2=0
                    b_bin2=extract_ans(MB_RX2,key_x_bin,bits_challenge)
                    print('Response to challenge: ; ',MB_RX2[96:128]) 
                    check2=numpy.dot(b_bin2,s_bin2)
                    count2=count2+1
                    if (check2==sum(z_bin2)):
                        incorrect2=incorrect2
                        print("\nAuthentic node. ", incorrect2, " absent or incorrect answer(s) in ", count2, "challenge(s) (SLAVE 2).\n")
                    else:
                        incorrect2=incorrect2+1
                        print("\nNot authentic node. ", incorrect2, " absent or >incorrect< answer(s) in ", count2, "challenge(s) (SLAVE 2).\n")
                    bDEC2=0
                    for bit in b_bin2:
                        bDEC2 = (bDEC2 << 1) | bit

            else:
                count2=count2+1
                incorrect2=incorrect2+1
                print("\nNot authentic node. ", incorrect2, " >absent< or incorrect answer(s) in ", count2, "challenge(s) (SLAVE 2).\n")
                noChallenge2=1
       #END OF SLAVE 2

        #SLAVE 3
        if (slaveIP==3):
            if (HB_MP=='y'):
                if (count3==0 or noChallenge3==1):
                    challenge3,challengeDEC3,challengeXOR3,challengeXORdec3,challengeBYTES3 =challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count3,MasterBbytes3,noChallenge3)    
                    sock.send(req+challengeBYTES3) #with challenge
                    print('\nChallenge: ; ',BitArray(challengeBYTES3))
                    s_bin3,z3,z_bin3=answer_gen (key_x_bin,challengeDEC3,bits_challenge,challenge3,key_x)
                if (count3>0 and noChallenge3==0):
                    s_bin3,z3,z_bin3=answer_gen (key_x_bin,bDEC3,bits_challenge,b_bin3,key_x)
                    MasterBbytes3=answer_noise(z3,bits_challenge,s_bin3,key_x_bin,z_bin3)
                    sock.send(req+MasterBbytes3)
                    print('Challenge: ; ', BitArray(MasterBbytes3))
                    challenge3,challengeDEC3,challengeXOR3,challengeXORdec3,challengeBYTES3=challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count3,MasterBbytes3,noChallenge3)
                    s_bin3,z3,z_bin3=answer_gen (key_x_bin,challengeDEC3,bits_challenge,challenge3,key_x)
            else:
                sock.send(req+padding)

            while True:
                rec = sock.recv(BUFFER_SIZE)
                MB_RX3=BitArray(rec)
                break
        
            if (len(MB_RX3)>100):
                if (HB_MP=='y'):
                    noChallenge3=0
                    b_bin3=extract_ans(MB_RX3,key_x_bin,bits_challenge)
                    print('Response to challenge: ; ',MB_RX3[96:128])
                    check3=numpy.dot(b_bin3,s_bin3)
                    count3=count3+1
                    if (check3==sum(z_bin3)):
                        incorrect3=incorrect3
                        print("\nAuthentic node. ", incorrect3, " absent or incorrect answer(s) in ", count3, "challenge(s) (SLAVE 3).\n")
                    else:
                        incorrect3=incorrect3+1
                        print("\nNot authentic node. ", incorrect3, " absent or >incorrect< answer(s) in ", count3, "challenge(s) (SLAVE 3).\n")
                    bDEC3=0
                    for bit in b_bin3:
                        bDEC3 = (bDEC3 << 1) | bit

            else:
                count3=count3+1
                incorrect3=incorrect3+1
                print("\nNot authentic node. ", incorrect3, " >absent< or incorrect answer(s) in ", count3, "challenge(s) (SLAVE 3).\n")
                noChallenge3=1
       #END OF SLAVE 3
            
        #SLAVE 4
        if (slaveIP==4):
            if (HB_MP=='y'):
                if (count4==0 or noChallenge4==1):
                    challenge4,challengeDEC4,challengeXOR4,challengeXORdec4,challengeBYTES4 =challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count4,MasterBbytes4,noChallenge4)    
                    sock.send(req+challengeBYTES4) #with challenge
                    print('\nChallenge: ; ',BitArray(challengeBYTES4))
                    s_bin4,z4,z_bin4=answer_gen (key_x_bin,challengeDEC4,bits_challenge,challenge4,key_x)
                if (count4>0 and noChallenge4==0):
                    s_bin4,z4,z_bin4=answer_gen (key_x_bin,bDEC4,bits_challenge,b_bin4,key_x)
                    MasterBbytes4=answer_noise(z4,bits_challenge,s_bin4,key_x_bin,z_bin4)
                    sock.send(req+MasterBbytes4)
                    print('Challenge: ; ', BitArray(MasterBbytes4))
                    challenge4,challengeDEC4,challengeXOR4,challengeXORdec4,challengeBYTES4=challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count4,MasterBbytes4,noChallenge4)
                    s_bin4,z4,z_bin4=answer_gen (key_x_bin,challengeDEC4,bits_challenge,challenge4,key_x)
            else:
                sock.send(req+padding)

            while True:
                rec = sock.recv(BUFFER_SIZE)
                MB_RX4=BitArray(rec)
                break
        
            if (len(MB_RX4)>100):
                if (HB_MP=='y'):
                    noChallenge4=0
                    b_bin4=extract_ans(MB_RX4,key_x_bin,bits_challenge)
                    print('Response to challenge: ; ',MB_RX4[96:128])
                    check4=numpy.dot(b_bin4,s_bin4)
                    count4=count4+1
                    if (check4==sum(z_bin4)):
                        incorrect4=incorrect4
                        print("\nAuthentic node. ", incorrect4, " absent or incorrect answer(s) in ", count4, "challenge(s) (SLAVE 4).\n")
                    else:
                        incorrect4=incorrect4+1
                        print("\nNot authentic node. ", incorrect4, " absent or >incorrect< answer(s) in ", count4, "challenge(s) (SLAVE 4).\n")
                    bDEC4=0
                    for bit in b_bin4:
                        bDEC4 = (bDEC4 << 1) | bit

            else:
                count4=count4+1
                incorrect4=incorrect4+1
                print("\nNot authentic node. ", incorrect4, " >absent< or incorrect answer(s) in ", count4, "challenge(s) (SLAVE 4).\n")
                noChallenge4=1
       #END OF SLAVE 4

        #SLAVE 5
        if (slaveIP==5):
            if (HB_MP=='y'):
                if (count5==0 or noChallenge5==1):
                    challenge5,challengeDEC5,challengeXOR5,challengeXORdec5,challengeBYTES5 =challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count5,MasterBbytes5,noChallenge5)    
                    sock.send(req+challengeBYTES5) #with challenge
                    print('\nChallenge: ; ',BitArray(challengeBYTES5))
                    s_bin5,z5,z_bin5=answer_gen (key_x_bin,challengeDEC5,bits_challenge,challenge5,key_x)
                if (count5>0 and noChallenge5==0):
                    s_bin5,z5,z_bin5=answer_gen (key_x_bin,bDEC5,bits_challenge,b_bin5,key_x)
                    MasterBbytes5=answer_noise(z5,bits_challenge,s_bin5,key_x_bin,z_bin5)
                    sock.send(req+MasterBbytes5)
                    print('Challenge: ; ', BitArray(MasterBbytes5))
                    challenge5,challengeDEC5,challengeXOR5,challengeXORdec5,challengeBYTES5=challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count5,MasterBbytes5,noChallenge5)
                    s_bin5,z5,z_bin5=answer_gen (key_x_bin,challengeDEC5,bits_challenge,challenge5,key_x)
            else:
                sock.send(req+padding)

            while True:
                rec = sock.recv(BUFFER_SIZE)
                MB_RX5=BitArray(rec)
                break
        
            if (len(MB_RX5)>100):
                if (HB_MP=='y'):
                    noChallenge5=0
                    b_bin5=extract_ans(MB_RX5,key_x_bin,bits_challenge)
                    print('Response to challenge: ; ',MB_RX5[96:128])
                    check5=numpy.dot(b_bin5,s_bin5)
                    count5=count5+1
                    if (check5==sum(z_bin5)):
                        incorrect5=incorrect5
                        print("\nAuthentic node. ", incorrect5, " absent or incorrect answer(s) in ", count5, "challenge(s) (SLAVE 5).\n")
                    else:
                        incorrect5=incorrect5+1
                        print("\nNot authentic node. ", incorrect5, " absent or >incorrect< answer(s) in ", count5, "challenge(s) (SLAVE 5).\n")
                    bDEC5=0
                    for bit in b_bin5:
                        bDEC5 = (bDEC5 << 1) | bit

            else:
                count5=count5+1
                incorrect5=incorrect5+1
                print("\nNot authentic node. ", incorrect5, " >absent< or incorrect answer(s) in ", count5, "challenge(s) (SLAVE 5).\n")
                noChallenge5=1
       #END OF SLAVE 5

        #SLAVE 6
        if (slaveIP==6):
            if (HB_MP=='y'):
                if (count6==0 or noChallenge6==1):
                    challenge6,challengeDEC6,challengeXOR6,challengeXORdec6,challengeBYTES6 =challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count6,MasterBbytes6,noChallenge6)    
                    sock.send(req+challengeBYTES6) #with challenge
                    print('\nChallenge: ; ',BitArray(challengeBYTES6))
                    s_bin6,z6,z_bin6=answer_gen (key_x_bin,challengeDEC6,bits_challenge,challenge6,key_x)
                if (count6>0 and noChallenge6==0):
                    s_bin6,z6,z_bin6=answer_gen (key_x_bin,bDEC6,bits_challenge,b_bin6,key_x)
                    MasterBbytes6=answer_noise(z6,bits_challenge,s_bin6,key_x_bin,z_bin6)
                    sock.send(req+MasterBbytes6)
                    print('Challenge: ; ', BitArray(MasterBbytes6))
                    challenge6,challengeDEC6,challengeXOR6,challengeXORdec6,challengeBYTES6=challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count6,MasterBbytes6,noChallenge6)
                    s_bin6,z6,z_bin6=answer_gen (key_x_bin,challengeDEC6,bits_challenge,challenge6,key_x)
            else:
                sock.send(req+padding)

            while True:
                rec = sock.recv(BUFFER_SIZE)
                MB_RX6=BitArray(rec)
                break
        
            if (len(MB_RX6)>100):
                if (HB_MP=='y'):
                    noChallenge6=0
                    b_bin6=extract_ans(MB_RX6,key_x_bin,bits_challenge)
                    print('Response to challenge: ; ',MB_RX6[96:128])
                    check6=numpy.dot(b_bin6,s_bin6)
                    count6=count6+1
                    if (check6==sum(z_bin6)):
                        incorrect6=incorrect6
                        print("\nAuthentic node. ", incorrect6, " absent or incorrect answer(s) in ", count6, "challenge(s) (SLAVE 6).\n")
                    else:
                        incorrect6=incorrect6+1
                        print("\nNot authentic node. ", incorrect6, " absent or >incorrect< answer(s) in ", count6, "challenge(s) (SLAVE 6).\n")
                    bDEC6=0
                    for bit in b_bin6:
                        bDEC6 = (bDEC6 << 1) | bit

            else:
                count6=count6+1
                incorrect6=incorrect6+1
                print("\nNot authentic node. ", incorrect6, " >absent< or incorrect answer(s) in ", count6, "challenge(s) (SLAVE 6).\n")
                noChallenge6=1
       #END OF SLAVE 6

        #SLAVE 7
        if (slaveIP==7):
            if (HB_MP=='y'):
                if (count7==0 or noChallenge7==1):
                    challenge7,challengeDEC7,challengeXOR7,challengeXORdec7,challengeBYTES7 =challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count7,MasterBbytes7,noChallenge7)    
                    sock.send(req+challengeBYTES7) #with challenge
                    print('\nChallenge: ; ',BitArray(challengeBYTES7))
                    s_bin7,z7,z_bin7=answer_gen (key_x_bin,challengeDEC7,bits_challenge,challenge7,key_x)
                if (count7>0 and noChallenge7==0):
                    s_bin7,z7,z_bin7=answer_gen (key_x_bin,bDEC7,bits_challenge,b_bin7,key_x)
                    MasterBbytes7=answer_noise(z7,bits_challenge,s_bin7,key_x_bin,z_bin7)
                    sock.send(req+MasterBbytes7)
                    print('Challenge: ; ', BitArray(MasterBbytes7))
                    challenge7,challengeDEC7,challengeXOR7,challengeXORdec7,challengeBYTES7=challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count7,MasterBbytes7,noChallenge7)
                    s_bin7,z7,z_bin7=answer_gen (key_x_bin,challengeDEC7,bits_challenge,challenge7,key_x)
            else:
                sock.send(req+padding)

            while True:
                rec = sock.recv(BUFFER_SIZE)
                MB_RX7=BitArray(rec)
                break
        
            if (len(MB_RX7)>100):
                if (HB_MP=='y'):
                    noChallenge7=0
                    b_bin7=extract_ans(MB_RX7,key_x_bin,bits_challenge)
                    print('Response to challenge: ; ',MB_RX7[96:128])
                    check7=numpy.dot(b_bin7,s_bin7)
                    count7=count7+1
                    if (check7==sum(z_bin7)):
                        incorrect7=incorrect7
                        print("\nAuthentic node. ", incorrect7, " absent or incorrect answer(s) in ", count7, "challenge(s) (SLAVE 7).\n")
                    else:
                        incorrect7=incorrect7+1
                        print("\nNot authentic node. ", incorrect7, " absent or >incorrect< answer(s) in ", count7, "challenge(s) (SLAVE 7).\n")
                    bDEC7=0
                    for bit in b_bin7:
                        bDEC7 = (bDEC7 << 1) | bit

            else:
                count7=count7+1
                incorrect7=incorrect7+1
                print("\nNot authentic node. ", incorrect7, " >absent< or incorrect answer(s) in ", count7, "challenge(s) (SLAVE 7).\n")
                noChallenge7=1
       #END OF SLAVE 7

        #SLAVE 8
        if (slaveIP==8):
            if (HB_MP=='y'):
                if (count8==0 or noChallenge8==1):
                    challenge8,challengeDEC8,challengeXOR8,challengeXORdec8,challengeBYTES8 =challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count8,MasterBbytes8,noChallenge8)    
                    sock.send(req+challengeBYTES8) #with challenge
                    print('\nChallenge: ; ',BitArray(challengeBYTES8))
                    s_bin8,z8,z_bin8=answer_gen (key_x_bin,challengeDEC8,bits_challenge,challenge8,key_x)
                if (count8>0 and noChallenge8==0):
                    s_bin8,z8,z_bin8=answer_gen (key_x_bin,bDEC8,bits_challenge,b_bin8,key_x)
                    MasterBbytes8=answer_noise(z8,bits_challenge,s_bin8,key_x_bin,z_bin8)
                    sock.send(req+MasterBbytes8)
                    print('Challenge: ; ', BitArray(MasterBbytes8))
                    challenge8,challengeDEC8,challengeXOR8,challengeXORdec8,challengeBYTES8=challenge_gen(key_x_bin,
                                                           bits_challenge, key_x,count8,MasterBbytes8,noChallenge8)
                    s_bin8,z8,z_bin8=answer_gen (key_x_bin,challengeDEC8,bits_challenge,challenge8,key_x)
            else:
                sock.send(req+padding)

            while True:
                rec = sock.recv(BUFFER_SIZE)
                MB_RX8=BitArray(rec)
                break
        
            if (len(MB_RX8)>100):
                if (HB_MP=='y'):
                    noChallenge8=0
                    b_bin8=extract_ans(MB_RX8,key_x_bin,bits_challenge)
                    print('Response to challenge: ; ',MB_RX8[96:128])
                    check8=numpy.dot(b_bin8,s_bin8)
                    count8=count8+1
                    if (check8==sum(z_bin8)):
                        incorrect8=incorrect8
                        print("\nAuthentic node. ", incorrect8, " absent or incorrect answer(s) in ", count8, "challenge(s) (SLAVE 8).\n")
                    else:
                        incorrect8=incorrect8+1
                        print("\nNot authentic node. ", incorrect8, " absent or >incorrect< answer(s) in ", count8, "challenge(s) (SLAVE 8).\n")
                    bDEC8=0
                    for bit in b_bin8:
                        bDEC8 = (bDEC8 << 1) | bit

            else:
                count8=count8+1
                incorrect8=incorrect8+1
                print("\nNot authentic node. ", incorrect8, " >absent< or incorrect answer(s) in ", count8, "challenge(s) (SLAVE 8).\n")
                noChallenge8=1
       #END OF SLAVE 8
        tempo_fim_ciclo.append(time.clock()-tempo_inicio_ciclo)
        print("\nCycle time ",tempo_fim_ciclo[i])
    finally:
        sock.close()



